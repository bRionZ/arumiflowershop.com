---
date: {{ .Date }}
title: "{{ replace .Name "-" " " | title }}"
description:  
id: "8"
image: /img/products/thumbnails/ysl-sac-de-jour-small.webp
categories: Saint Laurent
price: 6500
size: Onesize
color: Brown
material: Leather
season: All seasons
condition: Good
width: "35"
height: "25"
depth: "19"
handle: "10"
tags:
- saint-laurent
categories:
- Bags
---
