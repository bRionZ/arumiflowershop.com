import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'brands': {
    'backgroundColor': '#fff'
  },
  'footer-clean': {
    'backgroundColor': '#fff'
  },
  'brands': {
    'color': '#313437'
  },
  'brands a': {
    'display': 'block',
    'textAlign': 'center',
    'padding': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 0 }],
    '<w767': {
      'padding': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 0 }]
    }
  },
  'brands a img': {
    'display': 'inline-block',
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 20 }],
    'verticalAlign': 'middle'
  },
  'footer-clean': {
    'padding': [{ 'unit': 'px', 'value': 50 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 50 }, { 'unit': 'px', 'value': 0 }],
    'color': '#4b4c4d'
  },
  'footer-clean h3': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 12 }],
    'fontWeight': '700',
    'fontSize': [{ 'unit': 'px', 'value': 16 }]
  },
  'footer-clean ul': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'listStyle': 'none',
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'footer-clean ul a': {
    'color': 'inherit',
    'textDecoration': 'none',
    'opacity': '.8'
  },
  'footer-clean ul a:hover': {
    'opacity': '1'
  },
  'footer-clean itemsocial': {
    'textAlign': 'right',
    '<w768': {
      'textAlign': 'center'
    }
  },
  'footer-clean itemsocial>a': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'width': [{ 'unit': 'px', 'value': 40 }],
    'height': [{ 'unit': 'px', 'value': 40 }],
    'lineHeight': [{ 'unit': 'px', 'value': 40 }],
    'display': 'inline-block',
    'textAlign': 'center',
    'borderRadius': '50%',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'marginLeft': [{ 'unit': 'px', 'value': 10 }],
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'color': 'inherit',
    'opacity': '.75',
    '<w991': {
      'marginTop': [{ 'unit': 'px', 'value': 40 }]
    },
    '<w767': {
      'marginTop': [{ 'unit': 'px', 'value': 10 }]
    }
  },
  'footer-clean itemsocial>a:hover': {
    'opacity': '.9'
  },
  'footer-clean copyright': {
    'marginTop': [{ 'unit': 'px', 'value': 14 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'opacity': '.6'
  },
  'img': {
    'backgroundSize': 'cover',
    'backgroundRepeat': 'no-repeat'
  },
  'highlight-blue': {
    'color': '#fff',
    'backgroundColor': '#1e6add',
    'padding': [{ 'unit': 'px', 'value': 50 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 50 }, { 'unit': 'px', 'value': 0 }]
  },
  'highlight-blue p': {
    'color': '#c4d5ef',
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }]
  },
  'highlight-blue h2': {
    'fontWeight': '400',
    'marginBottom': [{ 'unit': 'px', 'value': 25 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'paddingTop': [{ 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'color': 'inherit'
  },
  'highlight-blue intro': {
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'maxWidth': [{ 'unit': 'px', 'value': 500 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'auto' }]
  },
  'highlight-blue buttons': {
    'textAlign': 'center'
  },
  'highlight-blue buttons btn': {
    'padding': [{ 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 32 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 32 }],
    'margin': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }],
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'background': '0 0',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'opacity': '.9',
    'textTransform': 'uppercase',
    'fontWeight': '700',
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'letterSpacing': [{ 'unit': 'px', 'value': 0.4 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }]
  },
  'highlight-blue buttons btn:hover': {
    'opacity': '1'
  },
  'highlight-blue buttons btn:active': {
    'transform': 'translateY(1px)'
  },
  'highlight-blue buttons btn-primary': {
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(255, 255, 255, .7)' }],
    'borderRadius': '6px',
    'color': '#ebeff1',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 24 }],
    'background': '0 0',
    'transition': 'background-color .25s'
  },
  'highlight-blue buttons btn-primary:active': {
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(255, 255, 255, .7)' }],
    'borderRadius': '6px',
    'color': '#ebeff1',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 24 }],
    'background': '0 0',
    'transition': 'background-color .25s'
  },
  'zhadow': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 18 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }]
  },
  'card:hover': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .2),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14)' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .14),' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 18 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, .12)' }]
  },
  'card card-title': {
    'color': '#363738'
  },
  'card price': {
    'color': '#363738'
  },
  'bg-gradient-primary': {
    'background': 'linear-gradient(80deg, #510fa8 0, #320fa8 100%) !important'
  },
  'bg-gradient-secondary': {
    'background': 'linear-gradient(80deg, #fafbfe 0, #fafcfe 100%) !important'
  },
  'bg-gradient-success': {
    'background': 'linear-gradient(80deg, #4cd964 0, #50d94c 100%) !important'
  },
  'bg-gradient-info': {
    'background': 'linear-gradient(80deg, #24b7fa 0, #24e2fa 100%) !important'
  },
  'bg-gradient-warning': {
    'background': 'linear-gradient(80deg, #ff9500 0, #ff6200 100%) !important'
  },
  'bg-gradient-danger': {
    'background': 'linear-gradient(80deg, #ff3b30 0, #ff304e 100%) !important'
  },
  'bg-gradient-light': {
    'background': 'linear-gradient(80deg, #ced4da 0, #ced6da 100%) !important'
  },
  'bg-gradient-dark': {
    'background': 'linear-gradient(80deg, #05172a 0, #051e2a 100%) !important'
  },
  'bg-gradient-white': {
    'background': 'linear-gradient(80deg, #fff 0, #fff 100%) !important'
  },
  'bg-gradient-darker': {
    'background': 'linear-gradient(80deg, #020b13 0, #020e13 100%) !important'
  },
  'headroom': {
    'transition': 'transform .25s ease-in-out',
    'willChange': 'transform'
  },
  'headroom--pinned': {
    'transform': 'translateY(0)'
  },
  'headroom--unpinned': {
    'transform': 'translateY(-100%)'
  },
  'sharing__button': {
    'fontSize': [{ 'unit': 'em', 'value': 0.8 }],
    'screen&&>w40': {
      'fontSize': [{ 'unit': 'em', 'value': 0.9 }]
    },
    'screen&&>w50': {
      'fontSize': [{ 'unit': 'em', 'value': 1 }]
    }
  },
  'question-set': {
    'border': [{ 'unit': 'string', 'value': 'none' }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'em', 'value': 1.5 }]
  },
  'question-set__title': {
    'display': 'block',
    'marginBottom': [{ 'unit': 'em', 'value': 0.75 }]
  },
  'question-set__input': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'padding': [{ 'unit': 'em', 'value': 0.25 }, { 'unit': 'em', 'value': 0.25 }, { 'unit': 'em', 'value': 0.25 }, { 'unit': 'em', 'value': 0.25 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#a4afba' }]
  },
  'question-set__input:focus': {
    'outline': 'none',
    'boxShadow': [{ 'unit': 'string', 'value': '#4ECDC4' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 0 }]
  },
  'question-set__label': {
    'display': 'inline-block',
    'marginBottom': [{ 'unit': 'em', 'value': 0.25 }]
  },
  'question-set__label--block': {
    'display': 'block'
  },
  'question-set__label input[type=radio]': {
    'marginRight': [{ 'unit': 'em', 'value': 0.75 }]
  },
  ':not(pre)>code': {
    'backgroundColor': '#d0d5db',
    'padding': [{ 'unit': 'em', 'value': 0.3 }, { 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.1 }, { 'unit': 'em', 'value': 0.2 }],
    'borderRadius': '.25em'
  },
  'p+h2': {
    'marginTop': [{ 'unit': 'em', 'value': 1.5 }]
  },
  'p+h3': {
    'marginTop': [{ 'unit': 'em', 'value': 1.5 }]
  },
  'complimentary': {
    'display': 'none',
    'screen&&>w40': {
      'display': 'inline'
    }
  },
  'subdued': {
    'opacity': '.6',
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'asubdued:hover': {
    'color': '#fff'
  },
  'downloads': {
    'marginBottom': [{ 'unit': 'em', 'value': 1.5 }]
  },
  'animated': {
    'WebkitAnimationDuration': '.5s',
    'MozAnimationDuration': '.5s',
    'OAnimationDuration': '.5s',
    'animationDuration': '.5s',
    'WebkitAnimationFillMode': 'both',
    'MozAnimationFillMode': 'both',
    'OAnimationFillMode': 'both',
    'animationFillMode': 'both',
    'willChange': 'transform, opacity'
  },
  'animatedslideDown': {
    'WebkitAnimationName': 'slideDown',
    'MozAnimationName': 'slideDown',
    'OAnimationName': 'slideDown',
    'animationName': 'slideDown'
  },
  'animatedslideUp': {
    'WebkitAnimationName': 'slideUp',
    'MozAnimationName': 'slideUp',
    'OAnimationName': 'slideUp',
    'animationName': 'slideUp'
  },
  'animatedswingInX': {
    'WebkitTransformOrigin': 'top',
    'MozTransformOrigin': 'top',
    'IeTransformOrigin': 'top',
    'OTransformOrigin': 'top',
    'transformOrigin': 'top',
    'WebkitBackfaceVisibility': 'visible !important',
    'WebkitAnimationName': 'swingInX',
    'MozBackfaceVisibility': 'visible !important',
    'MozAnimationName': 'swingInX',
    'OBackfaceVisibility': 'visible !important',
    'OAnimationName': 'swingInX',
    'backfaceVisibility': 'visible !important',
    'animationName': 'swingInX'
  },
  'animatedswingOutX': {
    'WebkitTransformOrigin': 'top',
    'WebkitAnimationName': 'swingOutX',
    'WebkitBackfaceVisibility': 'visible !important',
    'MozAnimationName': 'swingOutX',
    'MozBackfaceVisibility': 'visible !important',
    'OAnimationName': 'swingOutX',
    'OBackfaceVisibility': 'visible !important',
    'animationName': 'swingOutX',
    'backfaceVisibility': 'visible !important'
  },
  'animatedflipInX': {
    'WebkitBackfaceVisibility': 'visible !important',
    'WebkitAnimationName': 'flipInX',
    'MozBackfaceVisibility': 'visible !important',
    'MozAnimationName': 'flipInX',
    'OBackfaceVisibility': 'visible !important',
    'OAnimationName': 'flipInX',
    'backfaceVisibility': 'visible !important',
    'animationName': 'flipInX'
  },
  'animatedflipOutX': {
    'WebkitAnimationName': 'flipOutX',
    'WebkitBackfaceVisibility': 'visible !important',
    'MozAnimationName': 'flipOutX',
    'MozBackfaceVisibility': 'visible !important',
    'OAnimationName': 'flipOutX',
    'OBackfaceVisibility': 'visible !important',
    'animationName': 'flipOutX',
    'backfaceVisibility': 'visible !important'
  },
  'animatedbounceInDown': {
    'WebkitAnimationName': 'bounceInDown',
    'MozAnimationName': 'bounceInDown',
    'OAnimationName': 'bounceInDown',
    'animationName': 'bounceInDown'
  },
  'animatedbounceOutUp': {
    'WebkitAnimationName': 'bounceOutUp',
    'MozAnimationName': 'bounceOutUp',
    'OAnimationName': 'bounceOutUp',
    'animationName': 'bounceOutUp'
  }
});
