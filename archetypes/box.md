---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
description: "Toko bunga online 24 jam siap kirim Bunga {{ replace .Name "-" " " | title }}  dengan harga yang bersaing."
image: /img/koleksi/350/box.jpg
price: 500000
categories: "box"
type: "koleksi"
---
